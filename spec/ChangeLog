2014-12-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[dist] Distribute HTML spec, too.

	* GNUmakefile (HTML): New target.

2013-02-18  Thien-Thi Nguyen  <ttn@gnuvola.org>

	No longer distribute {epsf,texinfo}.tex.

	* epsf.tex: Delete file.
	* texinfo.tex: Likewise.

2013-02-18  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec int] Fix typo: Include "(news)" heading.

	Omission from 2013-02-16, "Add version 1.8 release notes".

	* ixin.texi (version 1.8): ...here.

2013-02-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.8

2013-02-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec int] Add version 1.8 release notes.

	* ixin.texi (Release Notes): Add "version 1.8" to menu.
	(version 1.8): New node/unnumberedsec.

2013-02-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Mention ‘mkixin’ infelicity for settings renderable sequence.

	* ixin.texi (settings): ...here, in table, w/ a new footnote.

2013-02-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Use custom headers/footers.

	* ixin.texi: ...here, after ‘@end titlepage’.

2013-02-15  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add release notes to The IXIN Chronicles.

	* ixin.texi (Top): Add "Release Notes" to menu.
	(Release Notes): New node/unnumbered.

2013-02-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify / handle tweaks, tweakrefs.

	* ixin.texi (settings): Update footnote; allow for zero or
	more VALUE in example; update ‘doubtindex’ and related ‘qqq’
	entries in table; move ‘@*’ to end of preceeding line.
	(node index): Remove footnote; allow for zero or more VALUE
	in example; reword NAME and VALUE blurb slightly.
	(node data): Add "tweakrefs" to menu.
	(tweakrefs): New node / subsection.

2013-02-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Use ‘real’ for ‘pagesizes’.

	* ixin.texi (settings): ...here, in table;
	remove ‘doubtindex’ entry and related ‘qqq’.

2013-02-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Add jargon: real

	* ixin.texi (reprensentations): ...here, in table.

2013-02-13  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Redistribute texinfo.tex from Texinfo 4.13.97.

	* texinfo.tex: New file.

2013-02-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Avoid ‘Top’ in ixcc example.

	This is for the sake of PDF (and other DVI) output,
	for which there is no ‘Top’ node.

	* ixin.texi (references): ...here, using ‘Introduction’
	and ‘Intro’, for IXIN and Emacs, respectively, instead.

2013-01-17  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Add some images to @copying, @titlepage.

	* logo-o3.png: New file.
	* ixin.texi <copying>: Add three ‘@image’s, in ‘@ifnotinfo’.
	<titlepage>: Add ‘@image’ between ‘@subtitle’ and ‘@author’.

2013-01-17  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify uniqueness of blobs.

	* ixin.texi (blobs index): ...here.

2013-01-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Drop support for makeinfo 4.13 and earlier.

	* ixin.texi (Technique): Don't mention old versions of makeinfo.
	(overall): Remove 4.13 DTD from example; update blurb.
	(xid): Don't mention makeinfo 4.13 deficiency.
	(titlepage): Don't mention makeinfo 4.13 ‘booktitle’.

2013-01-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Drop z4.

	* GNUmakefile (z4.xml): Delete target.
	* z4.README: Delete file.

2013-01-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec int] Add macros: hyphen, thesymhy

	* ixin.texi (hyphen, thesymhy): New macros;
	use them throughout to replace "@code{-} (hyphen)"
	and "the symbol @code{-} (hyphen)", respectively.

2013-01-14  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Change TYPE in BLOBS-INDEX entry to a list.

	* ixin.texi (blobs index): ...here, in table.

2013-01-11  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.7

2013-01-10  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add "retrieve repl" command: blob

	* ixin.texi (receive): Add ‘(blob N)’ to table.

2013-01-10  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Mention "Normal Form C"; add stuff from prob.xml.

	This is so we can eventually zonk the hand-rolled prob.xml
	in a future release, giving the distribution the property of
	"all XML generated via makeinfo".

	* ixin.texi (overall): ...here, in a footnote.

2013-01-09  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Move node base from README to spec doc; add blob base, too.

	* ixin.texi (node data): Add "node base" to menu.
	(node base): New node/subsection.
	(blobs): Describe how to calcuate "blob base".

2013-01-09  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify BLOBS-INDEX and BLOBS blocks.

	* ixin.texi (Specification): Add "blobs index" and "blobs" to menu.
	(counts): Add ‘BLOBS-INDEX-LEN’ to form; split form
	into two lines; add ‘blobs-index-len’ to table.
	(blobs index): New node/section.
	(node data): Remove "image inlining" from menu.
	(references): Add ‘blob’ to table of ‘ixcc’ families.
	(image inlining): Delete node/subsection.
	(blobs): New node/section.

2013-01-09  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Move implementation notes from README to spec doc.

	* ixin.texi (mkixin, retrieve): Add
	heading "implementation notes" and text.

2013-01-08  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Merge ‘vr’ index into ‘cp’; add many index terms.

	* ixin.texi (vmore, vmorex): New macros.
	Throughout, add many index terms.

2013-01-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Move utils description from README to spec document.

	* ixin.texi (Top): Add "Utilities" to menu.
	(Technique): Replace ‘@pindex’ for mkixin and makeinfo,
	with a ‘@cindex’ that includes the word "limitation";
	add ‘@pxref’ to ‘Utilities’.
	(Utilities): New node/chapter.

2013-01-07  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec int] Use ‘@format’ and ‘@center’ for floats.

	* ixin.texi (associative array): ...here.

2013-01-06  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Allow elts of META ... DESCRIPTION to be a renderable sequence.

	* ixin.texi <direntry>: Use ‘@sc’.
	(xid) <direntry>: Describe precisely the types for
	TITLE and NODE, including sub-structure for the latter;
	allow DESCRIPTION to also be a renderable sequence.

2013-01-06  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Rename spec document.

	* ixin.texi <header>: Change title to "The IXIN Chronicles".
	<header> (packageparticulars): New macro.
	<copying>: Update; use ‘@packageparticulars’.
	<direntry>: Update description.
	<titlepage>: Update.
	(Top): Update.

2013-01-03  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.6

2013-01-02  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Allow XID TITLE to be a renderable sequence.

	* ixin.texi (xid): ...here, adding a separate ‘@item’ for ‘title’.

2012-12-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.5

2012-12-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Say "byte-length" in table.

	* ixin.texi (representations):
	...here, in table, instead of "length".

2012-12-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec int] Remove redundant ‘@cindex encoding’.

	* ixin.texi (representations): ...here.

2012-12-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add z4.

	* GNUmakefile (z4.xml): New target.
	* z4.README: New file.

2012-12-30  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify / handle external reference targets aka labels.

	* ixin.texi (Specification): Add "labels" to menu.
	(counts): Add LABELS-LEN to form and table.
	(labels): New node/section.

2012-12-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Drop META ATTRS.

	* ixin.texi (meta): ...here, from form and table.
	(attrs): Delete node/subsection.

2012-12-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Use "-len" to save space.

	* ixin.texi: Throughout, except for
	"byte-length", replace foo-length with foo-len.

2012-12-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Drop COUNTS TOP.

	* ixin.texi (counts): ...here, from form and table.

2012-12-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[maint] Split out some rules into common.mk.

	* GNUmakefile: Include ../d/common.mk.
	(ixin.xml): Rename target from ‘../d/ixin.xml’.
	(../d/logo.png): Delete target.
	(check): Rewrite rule.

2012-12-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify / handle internal references.

	* ixin.texi (overall): Mention "no ‘ixcc’ in DTD", w/ ‘@pxref’.
	(node data): Add "references" to menu.
	(references): New node/subsection.

2012-12-29  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Move image inlining to its own node/subsection.

	* ixin.texi (node data): Add menu; move image inlining to...
	(image inlining): ...here, the new node/subsection; tweak
	‘@cindex’ entries; use ellipses in example's attributes.

2012-12-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Expand Program Index to include Doubts, as well.

	* ixin.texi (doubt): New ‘defindex’.
	(pg doubt): New ‘defsyncodeindex’.
	(Top): Rename "Program Index" to "Program and Doubt Index".
	(Technique): Change ‘@pindex’ entry from retrieve to makeinfo;
	mention makeinfo in the "limitations" sentence.
	(overall, attrs, vars, settings, counts, node)
	(data): Add ‘@aboutindex’ entries.
	(Program and Doubt Index): Rename from "Program Index";
	use ‘@printindex doubt’.

2012-12-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Don't specify / handle ‘copying’ attribute for ‘titlepage’.

	* ixin.texi (titlepage): ...here; rewrite.

2012-12-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Handle XML from makeinfo 4.13.92 (TexinfoML V5.00).

	* ixin.texi (overall): Update list of DTD public ids.

2012-12-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify / handle whitespace normalization.

	* ixin.texi (representations): Add "leaf string" to table.
	(overall): Add whitespace normalization rules: blurb + table.

2012-12-28  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Add section "overall".

	* ixin.texi (Specification): Add "overall" to menu;
	move 2nd paragraph to...
	(overall): ...here, the new node/section, and update it.

2012-12-27  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify / handle META XID INVITATIONS.

	This replaces 2012-12-26, "Specify / handle META XID DIRENTRIES",
	which failed to handle multiple or no given categories.

	* ixin.texi (xid): Replace DIRCATEGORY and DIRENTRIES
	with INVITATIONS in form; update table; add sub-form;
	clarify which are strings.

2012-12-26  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Default META COPYING to ‘-’ (hyphen).

	* ixin.texi (copying): ...here.

2012-12-26  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Drop POSITION from ‘float’ entry.

	* ixin.texi (float sets): ...here, from form and table.

2012-12-26  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify / handle META XID DIRENTRIES.

	* ixin.texi (xid): Add ‘DIRENTRIES’ to form;
	update blurb; add ‘direntries’ to table; remove @qqq doubt.

2012-12-26  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Use ‘-’ (hyphen) for unspecified XID element.

	* ixin.texi (xid): Update table.

2012-12-26  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[maint] Allow for makeinfo(1) and texi2pdf(1) override.

	Some flexability to help play around w/ different versions.

	* GNUmakefile (MAKEINFO, TEXI2PDF): New vars.
	(ixin.info, ../d/ixin.xml): Use $(MAKEINFO).
	(ixin.pdf): Use $(TEXI2PDF).

2012-12-25  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Don't bother w/ "(generally)" in jargonindex terms.

	* ixin.texi (ji): Delete macro.
	(more): In this macro, use ‘@jargonindex’ directly.
	(associative array): Use ‘@jargonindex’ directly.

2012-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Release: 1.4

2012-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify and handle LANG in META XID.

	* ixin.texi: Add ‘@documentlanguage en’.
	(xid): Add ‘LANG’ to form; rewrite simple
	paragraph as table; add ‘lang’ to table.
	(settings) <footnote>: Add ‘documentlanguage’.
	<table documentlanguage>: Update ‘type’ column.

2012-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify TWEAKS in NODE-INDEX elements.

	* ixin.texi (node index): ...here, w/ vetting gap footnote.

2012-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Add settings: everyheading, everyfooting

	* ixin.texi (settings): ...here, in table.

2012-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec int] Move suggestion to drop VARS from footnote to text.

	* ixin.texi (vars): ...here.

2012-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Ponder: What about ‘@direntry’?

	* ixin.texi (xid): ...here, as a @qqq doubt.

2012-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Replace "element" w/ "command" or "SXML".

	* ixin.texi (xid): Use "commands"; add ‘@’ to @code contents.
	(vars): Use "commands"; say "@@set".
	(counts): Likewise, w/ "@@printindex" and "@@listoffloats".
	(data): Use "SXML".

2012-12-22  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Move README "groupings" into spec doc.

	* ixin.texi (settings): Add vetting gap footnote;
	change two-entry table of form components into
	simple paragraph; add table of settings.

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Add jargon: renderable sequence

	* ixin.texi (representations): Add "renderable sequence" to table.
	(sectioning tree, document-term sets, float sets):
	Use "renderable sequence".

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec int] Use two-arg @uref.

	* ixin.texi (Technique): ...here, for IXIN homepage.

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Explain "vetting gaps"; add one for ‘image/*’.

	* ixin.texi (Technique): Explain "vetting gaps".
	(node data): Add footnote for ‘image/*’ vetting gap.

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] De-emphasize "input SXML"; suggest dropping META ATTRS.

	* ixin.texi (meta): Rewrite blurb.
	(attrs): Likewise; add @qqq doubt.

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec int] Decruft: Drop unused definition ‘A-SXML’.

	* ixin.texi (representations): ...here, from the table.

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Replace "(described below)" w/ @pxref.

	* ixin.texi (counts, node index): ...here.

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Say "node index" instead of "index".

	* ixin.texi (counts, node index): ...here.

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec] Move ‘block’ to head of jargon; use it more.

	* ixin.texi (representations): Move ‘block’ first.
	(Specification, first line, meta, counts, node index)
	(document-term sets, node data): Avoid "portion";
	don't mention "ends w/ newline" or "followed by newline";
	instead, use "block" or "series of blocks".

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[spec int] Add abstraction: qqq

	* ixin.texi (qqq): New macro.
	(Technique, Specification, vars, counts, node data): Use ‘qqq’.

2012-12-21  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Include "activity list" from README in spec doc.

	* GNUmakefile (input): Add todo.texi.
	(todo.texi): New target.
	* ixin.texi (Technique): Add blurb and @include todo.texi.

2012-12-20  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Say "sectioning tree" instead of "section tree".

	Suggested by Karl Berry.

	* ixin.texi (Specification): Use "sectioning tree" in menu.
	(count): Likewise, for table.
	(sectioning tree): Rename node from "section tree"; update.

2012-12-20  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify and handle "image inlining".

	* GNUmakefile (../d/logo.png): New target.
	(check): Add ../d/logo.png to prereq list.
	* ixin.texi (node data): Add subsection "image inlining".

2012-12-20  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add some @cindex to spec.

	* ixin.texi (Introduction, Design): ...here.

2012-12-20  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add logo image to spec doc.

	* logo.svg: New file.
	* logo.png: New file.
	* ixin.texi (Introduction): Add @image.

2012-12-20  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Redistribute epsf.tex from Texinfo 4.13.92.

	* epsf.tex: New file.

2012-12-20  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Suggest dropping TOP.

	* ixin.texi (counts) ...here, w/ a ‘???’ blurb.

2012-12-20  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Suggest dropping VARS.

	* ixin.texi (vars): ...here, as a ‘???’ footnote.

2012-12-19  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify and handle "float sets".

	* ixin.texi (Top): Rename "List of Floats" to "List of Figures" in menu.
	(associative array): Use @var in @shortcaption for first Figure.
	(Specification): Add "float sets" to menu.
	(counts): Add FLOSETS-INDEX to form, table.
	(float sets): New node/section.
	(List of Figures): Rename from "List of Floats".

2012-12-18  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Move S-TREE immediately after NODE-INDEX.

	* ixin.texi (Specification): Move "section tree" after "node index".
	(counts): Move ‘s-tree-length’ after ‘top’.
	(section tree): Move node after "node index".

2012-12-18  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc int] Add some floats and ‘@listoffloats’.

	* ixin.texi (Top): Add "List of Floats" to menu.
	(associative array): Add some figures, marked as ‘@float’.
	(List of Floats): New node/unnumbered.

2012-12-18  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Add jargon: length

	* ixin.texi (representations): Add ‘length’ to table.

2012-12-18  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify automagic conversion for old-style txi* vars.

	* ixin.texi (vars): ...here.

2012-12-18  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc int] Use @table more.

	* ixin.texi (vars, settings): ...in these places.

2012-12-18  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Change META last element to TOC.

	* ixin.texi (meta): Do ‘s/ELEMENT.../TOC/’ in form;
	do ‘s/meta elements/toc/’ in menu.
	(toc): Rename node/subsection from "meta elements"; rewrite.

2012-12-17  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Specify SXML ‘insertcopying’ instead of string.

	* ixin.texi (copying): ...here; update example form.

2012-12-17  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Call the DTS name ‘name’ instead of ‘type’.

	* ixin.texi (counts): ...here, in form and table.

2012-12-17  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add jargon: node identifier

	* ixin.texi (representations): Add "node identifier" to table.
	(counts): Say "NID" instead of "NI" in form, table;
	say "node identifier" for ‘top’ in table.
	(document-term sets): Likewise.
	(section tree): Likewise.

2012-12-17  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add DTS "default font" and "inverse default font".

	* ixin.texi (counts): Drop ‘DTS-COUNT’ from form,
	and ‘dts-count’ from table.
	(document-term sets): Specify appearance only for non-zero
	DTS-LENGTH; document initial form; add ‘-’ to entry form;
	add ‘count’ and ‘font’ to table.

2012-12-17  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc int] Fix bug: Move @ji after @item.

	* ixin.texi (Terminology) [more]: ...here, in this macro.

2012-12-16  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Replace term "tli" with "dts".

	* ixin.texi (Specifications): Rename "term indices"
	to "document-term sets" in menu.
	(counts): Rename ‘TLI’ to ‘DTS-INDEX’, ‘TLI-LENGTH’
	to ‘DTS-LENGTH’, and ‘TLI-COUNT’ to ‘DTS-COUNT’.
	(index): Update.
	(document-term sets): Rename node from "term indices"; update.

2012-12-15  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Add section: Terminology

	* ixin.texi <header>: Define index ‘jargon’; merge it into ‘cp’.
	(Top): Update blurb for "Introduction" menu item.
	(Introduction): Add "Terminology" to menu.
	(Terminology): New node/section.

2012-12-15  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Expand a bit on docfood and post-finalization hopes.

	* ixin.texi (Technique): ...here.  Also, add two ‘@cindex’ entries.

2012-12-15  Thien-Thi Nguyen  <ttn@gnuvola.org>

	[doc] Prefer ‘U+N’ over ‘ASCII N’.

	* ixin.texi (first line): Say ‘U+31’ instead of ‘ASCII 49, 0x31’.

2012-12-13  Thien-Thi Nguyen  <ttn@gnuvola.org>

	Add spec document.

	* GNUmakefile: New file.
	* fdl.texi: New file.
	* ixin.texi: New file.


Copyright (C) 2012 Thien-Thi Nguyen

Copying and distribution of this file, with or without modification,
are permitted provided the copyright notice and this notice are preserved.
